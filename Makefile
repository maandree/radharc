.POSIX:

CONFIGFILE = config.mk
include $(CONFIGFILE)

OS = linux
# Linux:   linux
# Mac OS:  macos
# Windows: windows
include mk/$(OS).mk


LIB_MAJOR = 1
LIB_MINOR = 3
LIB_VERSION = $(LIB_MAJOR).$(LIB_MINOR)
LIB_NAME = radharc


OBJ_RADHARC =\
	cg-base.o\
	radharc.o

HDR_RADHARC =\
	cg-base.h\
	libradharc.h


OBJ_RADHARC_IPC =\
	radharc-ipc.o

HDR_RADHARC_IPC =\
	libradharc.h


OBJ_LIBRADHARC =\
	libradharc.o

HDR_LIBRADHARC =\
	libradharc.h


LOBJ = $(OBJ_LIBRADHARC:.o=.lo)


CPPFLAGS_MACROS =\
	-D'PACKAGE_NAME="$(PACKAGE_NAME)"'\
	-D'COMMAND_NAME="$(COMMAND_NAME)"'


all: libradharc.a libradharc.$(LIBEXT) radharc radharc-ipc
$(OBJ_RADHARC): $(HDR_RADHARC)
$(OBJ_RADHARC_IPC): $(HDR_RADHARC_IPC)
$(OBJ_LIBRADHARC): $(HDR_LIBRADHARC)

.c.o:
	$(CC) -c -o $@ $< $(CPPFLAGS_MACROS) $(CPPFLAGS) $(CFLAGS)

.c.lo:
	$(CC) -fPIC -c -o $@ $< $(CFLAGS) $(CPPFLAGS)

radharc: $(OBJ_RADHARC)
	$(CC) -o $@ $(OBJ_RADHARC) $(LDFLAGS)

radharc-ipc: $(OBJ_RADHARC_IPC) libradharc.a
	$(CC) -o $@ $(OBJ_RADHARC_IPC) libradharc.a $(LDFLAGS_IPC)

libradharc.a: $(OBJ_LIBRADHARC)
	@rm -f -- $@
	$(AR) rc $@ $(OBJ_LIBRADHARC)
	$(AR) ts $@ > /dev/null

libradharc.$(LIBEXT): $(LOBJ)
	$(CC) $(LIBFLAGS) -o $@ $(LOBJ) $(LDFLAGS_LIB)

install: libradharc.a libradharc.$(LIBEXT) radharc radharc-ipc
	mkdir -p -- "$(DESTDIR)$(PREFIX)/bin"
	mkdir -p -- "$(DESTDIR)$(MANPREFIX)/man1"
	cp -- radharc radharc-ipc "$(DESTDIR)$(PREFIX)/bin/"
	cp -- radharc.1 radharc-ipc.1 "$(DESTDIR)$(MANPREFIX)/man1/"
	mkdir -p -- "$(DESTDIR)$(PREFIX)/lib"
	mkdir -p -- "$(DESTDIR)$(PREFIX)/include"
	cp -- libradharc.a "$(DESTDIR)$(PREFIX)/lib/"
	cp -- libradharc.$(LIBEXT) "$(DESTDIR)$(PREFIX)/lib/libradharc.$(LIBMINOREXT)"
	$(FIX_INSTALL_NAME) "$(DESTDIR)$(PREFIX)/lib/libradharc.$(LIBMINOREXT)"
	ln -sf -- libradharc.$(LIBMINOREXT) "$(DESTDIR)$(PREFIX)/lib/libradharc.$(LIBMAJOREXT)"
	ln -sf -- libradharc.$(LIBMAJOREXT) "$(DESTDIR)$(PREFIX)/lib/libradharc.$(LIBEXT)"
	cp -- libradharc.h "$(DESTDIR)$(PREFIX)/include/"

uninstall:
	-rm -f -- "$(DESTDIR)$(PREFIX)/bin/radharc"
	-rm -f -- "$(DESTDIR)$(PREFIX)/bin/radharc-ipc"
	-rm -f -- "$(DESTDIR)$(MANPREFIX)/man1/radharc.1"
	-rm -f -- "$(DESTDIR)$(MANPREFIX)/man1/radharc-ipc.1"
	-rm -f -- "$(DESTDIR)$(PREFIX)/lib/libradharc.a"
	-rm -f -- "$(DESTDIR)$(PREFIX)/lib/libradharc.$(LIBMAJOREXT)"
	-rm -f -- "$(DESTDIR)$(PREFIX)/lib/libradharc.$(LIBMINOREXT)"
	-rm -f -- "$(DESTDIR)$(PREFIX)/lib/libradharc.$(LIBEXT)"
	-rm -f -- "$(DESTDIR)$(PREFIX)/include/libradharc.h"

clean:
	-rm -f -- *.o *.a *.lo *.su *.so *.so.* *.dll *.dylib
	-rm -f -- *.gch *.gcov *.gcno *.gcda *.$(LIBEXT)
	-rm -f -- radharc radharc-ipc

.SUFFIXES:
.SUFFIXES: .lo .o .c

.PHONY: all check install uninstall clean
