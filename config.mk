PREFIX    = /usr
MANPREFIX = $(PREFIX)/share/man

CC = c99

CPPFLAGS    = -D_DEFAULT_SOURCE -D_BSD_SOURCE -D_XOPEN_SOURCE=700 -D_GNU_SOURCE
CFLAGS      = -Wall -O2
LDFLAGS     = -lcoopgamma -lred -lm -s
LDFLAGS_IPC = -s
LDFLAGS_LIB =

PACKAGE_NAME = radharc
COMMAND_NAME = radharc
